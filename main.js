"use strict"

// Реалізувати перемикання вкладок (таби) на чистому Javascript.
// Потрібно, щоб після натискання на вкладку відображався конкретний 
// текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. 
// У коментарях зазначено, який текст має відображатися для якої вкладки.

// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть 
// додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, 
// через такі правки не переставала працювати.

//length = 0 then push array

let listItems = Array.from(document.body.querySelectorAll('.tabs-title'));


let content = document.body.querySelector('.tabs-content');

let contentChildren = Array.from(content.children);

console.log(contentChildren[1]);

function activeListItem() {
    this.classList.add('active');
} 
function inactiveListItem() {
    this.classList.remove('active');
} 

function showText() {
    let index = listItems.indexOf(this);

    for (let i = 0; i < contentChildren.length; i++) {
        contentChildren[i].hidden = (i !== index~);
    }
}

for (let element of listItems) {

    element.addEventListener('focus', activeListItem);
    element.addEventListener('focus', showText);


    element.addEventListener('blur', inactiveListItem);
}